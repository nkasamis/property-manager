import auth0 from 'auth0-js';

export default class Auth {
    auth0 = new auth0.WebAuth({
        domain: 'nkasamis.auth0.com',
        clientID: 'wPrGuTAxq7yfd95D4GhgulIImETNpbCE',
        redirectUri: 'http://localhost:8080',
        responseType: 'token id_token',
        scope: 'openid'
    });

    login() {
        this.auth0.authorize();
    }
}