const React = require('react');

import PropertyItem from '../PropertyItem/PropertyItem';
//const PropertyItem = require('../PropertyItem/PropertyItem');

export default class PropertyList extends React.Component {
    render() {
        const properties = this.props.properties.map(property =>
            <PropertyItem key={property._links.self.href} property={property} />
        );
        return (
            <table>
                <tbody>
                <tr>
                    <th>Address</th>
                </tr>
                {properties}
                </tbody>
            </table>
        )
    }
}