const React = require('react');

export default class PropertyItem extends React.Component {
    render() {
        address_obj = this.props.property.address;
        const address_str = `${address_obj.number} ${address_obj.street} ${address_obj.city} ${address_obj.state} ${address_obj.zip}`;
        return (
            <tr>
                <td>{address_str}</td>
            </tr>
        )
    }
}