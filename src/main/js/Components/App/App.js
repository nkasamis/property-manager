const React = require('react');

const client = require('../../client');
//const PropertyList = require('../PropertyList/PropertyList');
import PropertyList from '../PropertyList/PropertyList';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {properties: []};
    }

    componentDidMount() {
        client({method: 'GET', path: '/api/properties'}).done(response => {
            this.setState({properties: response.entity._embedded.properties});
        });
    }

    render() {
        return (
            <PropertyList properties={this.state.properties} />
        )
    }
}