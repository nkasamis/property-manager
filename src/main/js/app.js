const React = require('react');
const ReactDOM = require('react-dom');

//const App = require('./Components/App/App');
import App from './Components/App/App';


ReactDOM.render(
    <App/>,
    document.getElementById('react')
);