package com.kasamis.propertymanager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DatabaseLoader implements CommandLineRunner {
    private final PropertyRepository repo;

    @Autowired
    public DatabaseLoader(PropertyRepository repo) {
        this.repo = repo;
    }

    @Override
    public void run(String... strings ) throws Exception {
        Landlord landlord1 = new Landlord();
        landlord1.setFirst_name("Nick");
        landlord1.setLast_name("Kasamis");
        landlord1.setEmail("nick@kasamis.com");

        Landlord landlord2 = new Landlord();
        landlord2.setFirst_name("Steve");
        landlord2.setLast_name("Kasamis");
        landlord2.setEmail("steve@kasamis.com");

        Tenant tenant1 = new Tenant();
        tenant1.setFirst_name("Joe");
        tenant1.setLast_name("Donuts");
        tenant1.setEmail("joe@gmail.com");

        Tenant tenant2 = new Tenant();
        tenant2.setFirst_name("Eddie");
        tenant2.setLast_name("Sullivan");
        tenant2.setEmail("ed.sully@gmail.com");

        Tenant tenant3 = new Tenant();
        tenant3.setFirst_name("James");
        tenant3.setLast_name("Dean");
        tenant3.setEmail("rebelwnocause@gmail.com");

        Address address1 = new Address(
                "46",
                "Hickory Lane",
                "Garnerville",
                "NY",
                "10923");

        Address address2 = new Address(
                "118",
                "Filors Lane",
                "Stony Point",
                "NY",
                "10980"
        );

        Address address3 = new Address(
                "333",
                "Spooky Lane",
                "Stony Point",
                "NY",
                "10980"
        );

        Property property1 = new Property();
        property1.setAddress(address1);
        property1.setLandlord(landlord1);
        property1.addTenant(tenant1);
        property1.addTenant(tenant2);

        Property property2 = new Property();
        property2.setAddress(address2);
        property2.setLandlord(landlord1);
        property2.addTenant(tenant3);

        Property property3 = new Property();
        property3.setAddress(address3);
        property3.setLandlord(landlord2);

        this.repo.save(property1);
        this.repo.save(property2);
        this.repo.save(property3);
    }
}
