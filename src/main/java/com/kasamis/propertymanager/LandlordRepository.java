package com.kasamis.propertymanager;

import org.springframework.data.repository.CrudRepository;

public interface LandlordRepository extends CrudRepository<Landlord, Long> {
}
