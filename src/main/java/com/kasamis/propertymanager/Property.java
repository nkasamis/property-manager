package com.kasamis.propertymanager;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
//@Inheritance(strategy = InheritanceType.JOINED)
public class Property {

    private @Id @GeneratedValue Long id;
    private String type;
    private @OneToOne(cascade = CascadeType.ALL) Address address;
    private @OneToMany(cascade = CascadeType.ALL, mappedBy = "property") List<Tenant> tenants;
    private @ManyToOne(cascade = CascadeType.ALL) Landlord landlord;

    public Property() {
        this.tenants = new ArrayList<Tenant>();

    }

    public Property (Address address, Landlord landlord, String type) {
        this.address = address;
        this.landlord = landlord;
        this.tenants = new ArrayList<Tenant>();
        this.type = type;
    }

    /*public Property (Address address, Landlord landlord, List<Tenant> tenants) {
        super();
        this.address = address;
        this.landlord = landlord;
        this.tenants = tenants;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Landlord getLandlord() {
        return landlord;
    }

    public void setLandlord(Landlord landlord) {
        if(sameAsFormer(landlord)) {
            return;
        }
        Landlord oldLandlord = this.landlord;
        this.landlord = landlord;

        if(oldLandlord != null) {
            oldLandlord.removeProperty(this);
        }
        if(landlord != null) {
            landlord.addProperty(this);
        }
    }

    public boolean sameAsFormer(Landlord newLandlord) {
        return landlord==null? newLandlord == null : landlord.equals(newLandlord);
    }

    public List<Tenant> getTenants() {
        return new ArrayList<Tenant>(tenants);
    }

    public void addTenant(Tenant tenant) {
        if(tenants.contains(tenant)) {
            return;
        }

        tenants.add(tenant);
        tenant.setProperty(this);
    }

    public void removeTenant(Tenant tenant) {
        if(!tenants.contains(tenant)) {
            return;
        }

        tenants.remove(tenant);
        tenant.setProperty(null);
    }
}
