package com.kasamis.propertymanager;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Landlord {
    private @Id @GeneratedValue Long id;
    private String first_name;
    private String last_name;
    private String email;
    private @OneToMany(cascade = CascadeType.ALL, mappedBy = "landlord") List<Property> properties = new ArrayList<Property>();

    public Landlord() {
        //super();
    }

    public Landlord(String first_name, String last_name, String email) {
        //super(first_name, last_name, email);
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        //this.properties = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Property> getProperties() {
        return new ArrayList<Property>(properties);
    }

    public void addProperty(Property property) {
        if(properties.contains(property)) {
            return;
        }

        properties.add(property);
        property.setLandlord(this);
    }

    public void removeProperty(Property property) {
        if(!properties.contains(property)) {
            return;
        }

        properties.remove(property);
        property.setLandlord(null);
    }

    @Override
    public String toString() {
        return this.getFirst_name() + " " + this.getLast_name();
    }

}
