package com.kasamis.propertymanager;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Tenant {
    private @Id @GeneratedValue Long id;
    private String first_name;
    private String last_name;
    private String email;
    private @ManyToOne(cascade = CascadeType.ALL) Property property;

    public Tenant() {
        super();
    }

    public Tenant(String first_name, String last_name, String email) {
        //super(first_name, last_name, email);
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
    }

    public Tenant(String first_name, String last_name, String email, Property property) {
        //super(first_name, last_name, email);
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;

        setProperty(property);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        if(sameAsFormer(property)) {
            return;
        }

        Property oldProperty = this.property;
        this.property = property;

        if(oldProperty != null) {
            oldProperty.removeTenant(this);
        }
        if(property != null) {
            property.addTenant(this);
        }
    }

    public boolean sameAsFormer(Property newProperty) {
        return property == null? newProperty == null : property.equals(newProperty);
    }

    @Override
    public String toString() {
        return this.getFirst_name() + " " + this.getLast_name();
    }

}
