package com.kasamis.propertymanager;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity
public class House extends Property {

    public House() {
        super();
    }

    public House(Address address, Landlord landlord) {
        super(address, landlord, "house");
    }

}
