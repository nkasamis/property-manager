package com.kasamis.propertymanager;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Address {
    private @Id @GeneratedValue Long id;
    private String number;
    private String unit;
    private String street;
    private String city;
    private String state;
    private String zip;

    public Address(String number, String unit, String street, String city, String state, String zip) {
        this.number = number;
        this.unit = unit;
        this.street = street;
        this.city = city;
        this.state = state;
        this.zip = zip;
    }

    public Address(String number, String street, String city, String state, String zip) {
        this.number = number;
        this.street = street;
        this.city = city;
        this.state = state;
        this.zip = zip;
    }
}
